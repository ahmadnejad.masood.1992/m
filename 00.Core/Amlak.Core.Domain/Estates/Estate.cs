﻿using Amlak.Core.Domain.Owners;
using System;

namespace Amlak.Core.Domain.Estates
{
    public class Estate
    {
        public int EstateId { get; set; }
        public int UniqueId { get; set; }
        public string Title { get; set; }
        public int Area { get; set; }
        public string Address { get; set; }
        public bool AreaType { get; set; }
        public string Description { get; set; }

        public DateTime CreateDateEn { get; set; }
        public string CreateDateFa { get; set; }

        public DateTime? LastEditDateEn { get; set; }
        public string LastEditDateFa { get; set; }

        public int EditedUserId { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Owner Owner { get; set; }
    }
}
