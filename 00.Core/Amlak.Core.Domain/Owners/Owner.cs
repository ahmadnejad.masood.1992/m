﻿using Amlak.Core.Domain.Estates;
using System.Collections.Generic;

namespace Amlak.Core.Domain.Owners
{
    public class Owner
    {
        public int OwnerId { get; set; }
        public int EstateId { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public string PhoneNumber { get; set; }
        public virtual IEnumerable<Estate> Estates { get; set; }
    }
}
