﻿using Amlak.Core.Domain.Estates;
using Amlak.Core.Domain.Owners;
using Microsoft.EntityFrameworkCore;

namespace Amlak.Infrastructures.Data.SqlServer
{
    public class AmlakDbContext : DbContext
    {
        public AmlakDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Estate> Estates { get; set; }
        public DbSet<Owner> Owners { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
        }

    }
}
