﻿using System.Linq;
using System.Threading.Tasks;

namespace Amlak.Infrastructures.Data.SqlServer.Commons
{
    public interface IRepository<TEntity> where TEntity : class, new()
    {
        IQueryable<TEntity> GetAll();
        Task<TEntity> AddAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task<TEntity> SoftDeleteAsync(TEntity entity);
    }

}
