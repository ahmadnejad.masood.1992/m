﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amlak.Infrastructures.Data.SqlServer.Commons
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        protected readonly AmlakDbContext AmlakDbContext;

        public Repository(AmlakDbContext amlakDbContext)
        {
            AmlakDbContext = amlakDbContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            try
            {
                return AmlakDbContext.Set<TEntity>();
            }
            catch (Exception ex)
            {
                throw new Exception($"خطایی ایجاد شده است: {ex.Message}");
            }
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} نباید خالی باشد");
            }

            try
            {
                await AmlakDbContext.AddAsync(entity);
                await AmlakDbContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} نمیتواند ذخیره شود: {ex.Message}");
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(UpdateAsync)} نباید خالی باشد");
            }
            try
            {
                AmlakDbContext.Update(entity);
                await AmlakDbContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} نمیتواند بروزرسانی شود: {ex.Message}");
            }
        }

        public async Task<TEntity> SoftDeleteAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(SoftDeleteAsync)} نباید خالی باشد");
            }

            try
            {
                AmlakDbContext.Update(entity);
                await AmlakDbContext.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} نمیتواند بروزرسانی شود: {ex.Message}");
            }
        }
    }
}
