﻿using Amlak.Core.Domain.Estates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amlak.Infrastructures.Data.SqlServer.Estates.Configs
{
    public class EstateConfig : IEntityTypeConfiguration<Estate>
    {
        public void Configure(EntityTypeBuilder<Estate> builder)
        {
            builder.ToTable("Estates", "rs");
            builder.HasKey(x => x.EstateId);
            builder.Property(x => x.EstateId).IsRequired().ValueGeneratedOnAdd();
            builder.Property(x => x.UniqueId).IsRequired().HasDefaultValueSql("NEXT VALUE FOR rs.EstateUniqueIdSequence"); ;
            builder.Property(x => x.Title).IsRequired().HasMaxLength(100);
            builder.Property(x => x.Area).IsRequired();
            builder.Property(x => x.Address).IsRequired().HasMaxLength(250);
            builder.Property(x => x.AreaType).IsRequired();
            builder.Property(x => x.Description).IsRequired().HasMaxLength(250);

            builder.Property(x => x.CreateDateEn).IsRequired();
            builder.Property(x => x.CreateDateFa).IsRequired().HasMaxLength(10);

            builder.Property(x => x.LastEditDateEn).IsRequired(false);
            builder.Property(x => x.LastEditDateFa).IsRequired(false).HasMaxLength(10);

            builder.Property(x => x.EditedUserId).IsRequired().HasDefaultValue(0);
            builder.Property(x => x.IsDeleted).IsRequired().HasDefaultValue(false);

            
        }
    }
}
