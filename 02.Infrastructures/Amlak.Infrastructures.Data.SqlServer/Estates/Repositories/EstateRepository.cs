﻿using Amlak.Core.Domain.Estates;
using Amlak.Infrastructures.Data.SqlServer.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amlak.Infrastructures.Data.SqlServer.Estates.Repositories
{
    public class EstateRepository : Repository<Estate>, IEstateRepository
    {
        public EstateRepository(AmlakDbContext amlakDbContext) : base(amlakDbContext)
        {
        }

    }
}
