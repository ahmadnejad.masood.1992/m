﻿using Amlak.Core.Domain.Estates;
using Amlak.Infrastructures.Data.SqlServer.Commons;
using System.Threading.Tasks;

namespace Amlak.Infrastructures.Data.SqlServer.Estates.Repositories
{
    public interface IEstateRepository : IRepository<Estate>
    {
    }
}
